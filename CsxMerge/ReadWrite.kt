import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

sealed class ReadWrite {
    companion object {
        private val homeDir = System.getProperty("user.dir")

        fun readFromFile(fileName: String): String {
            try {
                val file = File("$homeDir/$fileName")
                val fis = FileInputStream(file)
                val data = ByteArray(file.length().toInt())
                fis.read(data)
                fis.close()
                return String(data, Charsets.UTF_8)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return ""
        }

        fun readAllFromFolder(folderName: String): List<String> {
            try {
                val folder = File("$homeDir/$folderName")
                val files = folder.listFiles() ?: throw Error("No files in folder")
                val fileNamesInFolder = files.filterNot{ it.isDirectory }.map { it.name }
                return fileNamesInFolder.map { readFromFile(fileName = "$folderName/$it") }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return emptyList()
        }

        fun writeToFile(fileName: String, content: String, append: Boolean = false) {
            try {
                val file = File("$homeDir/$fileName")
                val fos = FileOutputStream(file, append)
                fos.write(content.toByteArray())
                fos.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }
}

