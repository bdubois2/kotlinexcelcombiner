import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.streaming.SXSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileInputStream
import java.io.FileOutputStream
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@DslMarker
annotation class PoinkDsl

@PoinkDsl
class PSheet(
    private val sheet: Sheet
) : Sheet by sheet {
    private val format = workbook.createDataFormat()
    private val dateFormat = format.getFormat("MM/DD/YYYY")
    private val timeStampFormat = format.getFormat("MM/DD/YYYY HH:MM:SS")
    private val calendarFormat = format.getFormat("MMM D, YYYY")

    /**
     * Add objects as a new row at bottom of sheet. These objects will be intelligently rendered based on [String],
     * [Number], [LocalDate], [LocalDateTime], [Calendar] or else the [Object.toString].
     * @param cells A [List] of [Any] to add to [Sheet] as a row.
     * @param style An optional [CellStyle] for the cells in the row.
     * @return The [List] of [Cell] added as a row.
     */
    fun row(cells: List<Any>, style: CellStyle? = null): List<Cell> {
        val cellList = mutableListOf<Cell>()
        val row = sheet.createRow(sheet.physicalNumberOfRows)
        var col = 0
        cells.forEach { cellValue ->
            cellList.add(
                row.createCell(col++).apply {
                    cellStyle = style
                    when (cellValue) {
                        is String -> setCellValue(cellValue)
                        is Number -> setCellValue(cellValue.toDouble())
                        is LocalDateTime -> {
                            cellStyle = cloneAndFormat(style, timeStampFormat)
                            setCellValue(cellValue)
                        }
                        is LocalDate -> {
                            cellStyle = cloneAndFormat(style, dateFormat)
                            setCellValue(cellValue)
                        }
                        is Calendar -> {
                            cellStyle = cloneAndFormat(style, calendarFormat)
                            setCellValue(cellValue)
                        }
                        else -> setCellValue(cellValue.toString())
                    }
                }
            )
        }
        return cellList
    }

    private fun cloneAndFormat(style: CellStyle?, format: Short) = workbook.createCellStyle().apply {
        if (style != null) {
            cloneStyleFrom(style)
        }
        dataFormat = format
    }
}


@PoinkDsl
class PWorkbook(private val workbook: XSSFWorkbook = XSSFWorkbook()) : Workbook by workbook {
    private val styles: MutableMap<String, XSSFCellStyle> = mutableMapOf()

    /**
     * Get a named sheet, or create if absent, in the workbook.
     * @param name of the new sheet, will default to "Sheet #"
     * @param block Code to perform on the sheet.
     * @return The existing sheet, or a new one if named sheet doesn't exit.
     */
    fun sheet(name: String = "Sheet ${numberOfSheets + 1}", block: PSheet.() -> Unit) =
        PSheet(getSheet(name) ?: workbook.createSheet(name)).apply(block)

    /**
     * Get an existing sheet by its index.
     * @param index The index of the sheet
     * @param block Code to perform on the sheet.
     * @return Sheet at a given index.
     */
    fun sheet(index: Int, block: PSheet.() -> Unit) = PSheet(workbook.getSheetAt(index)).apply(block)

    /**
     * Get a named [CellStyle], or create if absent, in the workbook for future use.
     * @param name to use for created style.
     * @param block Lambda to apply to the created style.
     * @return The created [CellStyle]
     */
    fun cellStyle(name: String, block: CellStyle.() -> Unit = {}): CellStyle =
        styles.computeIfAbsent(name) { workbook.createCellStyle() as XSSFCellStyle }
            .apply(block)

    /**
     * Write the workbook out to a file.
     * @param path name of the output file.
     */
    fun write(path: String) = FileOutputStream(path).use { workbook.write(it) }
}

/**
 * Create a workbook.
 * @param block Code to perform on the workbook.
 * @return A new workbook.
 */
fun workbook(block: PWorkbook.() -> Unit): PWorkbook = PWorkbook().apply(block)

/**
 * Open existing workbook.
 * @param path File path to an existing workbook.
 * @param block Code to perform on the workbook.
 * @return The loaded from the provided file.
 */
fun workbook(path: String, block: PWorkbook.() -> Unit): PWorkbook =
    FileInputStream(path).use {
        val xssfWorkbook = XSSFWorkbook(it)
        PWorkbook(xssfWorkbook).apply(block)
    }
