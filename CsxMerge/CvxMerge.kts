#!/usr/bin/env kscript
@file:KotlinOptions("-J-Xmx9g")
@file:Import("ReadWrite.kt")
@file:Import("PoinkDsl.kt")
@file:DependsOn("org.apache.poi:poi:5.2.3")
@file:DependsOn("org.apache.poi:poi-ooxml:5.2.3")
import org.apache.poi.ss.usermodel.Row


val inputName = args.first()
val mergeFileName = "MergeResult"
if(inputName.endsWith(".xlsx")){
    println("~~~~~~~~ Reading workbook...")
    val rowCollection = mutableListOf<Row>()
    workbook(inputName) {
        repeat(numberOfSheets) { sheetIndex ->
            sheet(sheetIndex) {
                iterator().forEach { row ->
                    rowCollection.add(row)
                }
            }
        }
    }
    println("~~~~~~~~ Merging Sheets...")
    workbook {
        sheet("Merged") {
            rowCollection.forEach {
                val cells = it.cellIterator().asSequence().toList()
                row(cells = cells)
            }
        }
        println("~~~~~~~~ Creating Output file...")
    }.write("$mergeFileName.xlsx")
}else {
    ReadWrite.apply {
        println("~~~~~~~~ Reading Files...")
        val folderContents = readAllFromFolder(folderName = inputName)
        val fileName =  "$inputName/$mergeFileName.csv"
        println("~~~~~~~~ Joining csv's...")
        folderContents.forEach { content ->
            writeToFile(
                fileName = fileName,
                content = content,
                append = true
            )
        }
    }
}
println("~~~~~~~~ CsxMerge complete ~~~~~~")
