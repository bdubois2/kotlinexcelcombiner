
This is an excel reader based on Poink DSL and Apache Poi library. It is intended to combine sheets inside of a large excel file into one big sheet, but can be adapted for working with excel in many ways. 

## Running
This project is based on kscript and compiles into a standard binary excutable. You can run this from the command line in most Unix environments.

`./CvxMerge book1` Will take a folder of CSVs and merge them into a single excel file. 

Running with a file with a `.xlsx` extension will automatically merge all of the sheets for you.

To bootstrap an Intellij project (for working with the code) use this command:

`kscript --idea ./CvxMerge.kts`

Go to edit configurations and add your file's full path to the program arguments.

### Future plans:
Coroutines and streaming to work with very large files
More features and better CLI interface


